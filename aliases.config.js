const path = require('path');

function resolveSrc(_path) {
  return path.join(__dirname, _path);
}

const aliases = {
  '@': 'src',
  '@src': 'src',
};

const result = {
  webpack: {},
  jest: {},
};

Object.keys(aliases).forEach((alias) => {
  result.webpack[alias] = resolveSrc(aliases[alias]);
  result.jest[`^${alias}/(.*)$`] = `<rootDir>/${aliases[alias]}/$1`;
});

module.exports.webpack = result.webpack;
module.exports.jest = result.jest;
