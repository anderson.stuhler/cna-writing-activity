import WritingInstructions from '../components/Instructions/Index';
import WritingAnswer from '../components/Answer/Index';
import WritingFeedback from '../components/Feedback/Index';

export default {
  components: {
    WritingInstructions,
    WritingAnswer,
    WritingFeedback,
  },
  props: {
    activeTab: {
      type: Number,
      default: 0,
    },
    concluded: {
      type: Boolean,
      default: false,
    },
    activity: {
      type: Object,
    },
    feedback: {
      type: Object,
    },
    text: {
      type: String,
      default: '',
    },
    slideName: {
      type: String,
      default: 'slide-left',
    },
  },
  methods: {
    setAnswer(data) {
      this.$emit('input', data);
    },
  },
};
