import { shallowMount } from '@vue/test-utils';
import WritingHeader from '../WritingHeader';

const wrapper = shallowMount(WritingHeader, {
  mocks: {
    $vuetify: { breakpoint: {} },
  },
});

describe('Component to show title and actions of header', () => {
  test('Should close tooltip header in mobile', () => {
    wrapper.vm.showTooltip();
    expect(wrapper.vm.tooltip).toBeTruthy();
  });
  test('Should emit the exit activity', () => {
    wrapper.vm.exit();
    expect(wrapper.emitted()).toHaveProperty('exit');
  });
});
