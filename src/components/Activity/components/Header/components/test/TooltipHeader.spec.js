import { shallowMount } from '@vue/test-utils';
import TooltipHeader from '../TooltipHeader';

const wrapper = shallowMount(TooltipHeader, {
  mocks: {
    $vuetify: { breakpoint: {} },
  },
});

describe('Component to show title and actions of header', () => {
  test('Should close tooltip header in mobile', () => {
    wrapper.vm.showTooltip();
    expect(wrapper.vm.tooltip).toBeTruthy();
  });
});
