import { shallowMount } from '@vue/test-utils';
import stubs from '@/tests/unit-config/vuetifyStubs';

import Answer from '../Index';

const wrapper = shallowMount(Answer, {
  stubs,
  mocks: {
    $vuetify: {
      breakpoint: {},
    },
  },
});

describe('Component to write the answer', () => {
  test('should emit the answer', () => {
    wrapper.vm.setText({ target: { value: 'this is an answer' } });
    expect(wrapper.emitted().input[0][0]).toEqual('this is an answer');
  });
});
