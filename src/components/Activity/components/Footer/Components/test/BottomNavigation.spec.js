import { shallowMount } from '@vue/test-utils';
import stubs from '@/tests/unit-config/vuetifyStubs';
import BottomNavigation from '../BottomNavigation';

const wrapper = shallowMount(BottomNavigation, {
  stubs,
  mocks: {
    $vuetify: {
      breakpoint: {},
    },
  },
});

describe('component to change type of instruction', () => {
  test('Should set as active the tab ', () => {
    wrapper.vm.selectedItem(1);
    expect(wrapper.emitted()).toHaveProperty('selected');
  });
});
