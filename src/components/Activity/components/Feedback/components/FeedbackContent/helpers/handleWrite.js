export function tabsOrderList(it) {
  const orders = {
    KEYWORD: 0,
    VOCABULARY: 1,
    GRAMMAR: 2,
    SPELLING: 3,
  };
  return orders[it];
}
export function handleItemstoCarousel(content, tabs) {
  return content.reduce((acc, value) => {
    const index = tabs.findIndex(it => it.value === value.rule);
    acc.push({
      ...value,
      next: tabs[index].order,
    });

    return acc.sort((a, b) => a.next - b.next);
  }, []);
}
export function createTabs(prev, cur, content) {
  const index = content.findIndex(it => it.rule === cur);
  return {
    value: cur,
    amount: prev[cur] ? prev[cur].amount + 1 : 1,
    color: content[index].color || 'transparent',
    order: tabsOrderList(cur),
  };
}
export function handleTabs(data) {
  const tabs = data
    ? data
      .map(item => item.rule)
      .reduce((prev, cur) => {
        prev[cur] = createTabs(prev, cur, data);
        return prev;
      }, {})
    : [];
  return Object.keys(tabs)
    .map(it => tabs[it])
    .sort((a, b) => a.order - b.order);
}
export function cloneDeep(data) {
  return JSON.parse(JSON.stringify(data));
}
export function getReplacements(data = []) {
  return data.replacements && data.replacements.length > 0
    ? data.replacements.map(it => it.value)
    : [];
}
export function findTextByOffset(data, item) {
  if (item.offset >= 0 && item.length > 0) {
    return {
      ...item,
      order: item.offset,
      replacements: item.replacements,
      value: data.substring(item.offset, item.offset + item.length),
    };
  }
  return {
    ...item,
    order: -1,
    value: item.sentence,
    rule: 'KEYWORD',
  };
}
export function checkTextInCurrentWord(index, current) {
  return (
    current.offset !== null && index >= current.offset && index <= current.length + current.offset
  );
}
export function isLetterInWords(index, words) {
  let contain = false;
  for (let i = 0; i < words.length; i += 1) {
    const current = words[i];
    if (checkTextInCurrentWord(index, current)) {
      contain = true;
      break;
    }
  }
  return contain;
}
export function handleText(sentence, content) {
  const words = [];
  const text = { offset: null, value: '' };
  for (let i = 0; i < content.length; i += 1) {
    words.push(findTextByOffset(sentence, content[i]));
  }
  for (let i = 0; i < sentence.length; i += 1) {
    if (isLetterInWords(i, words)) {
      if (text.value.length > 0) {
        words.push(cloneDeep(text));
        text.value = '';
        text.offset = null;
      }
      continue;
    }
    text.value += sentence[i];
    text.offset = text.offset === null ? i : text.offset;
    text.order = text.offset;
    if (i === sentence.length - 1) {
      words.push(text);
    }
  }
  return words.sort((a, b) => a.order - b.order);
}
