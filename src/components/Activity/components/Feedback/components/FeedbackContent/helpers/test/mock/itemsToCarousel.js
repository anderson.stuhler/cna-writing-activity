export default function getItems() {
  return [
    {
      value: 'again',
      order: 12,
      feedback: "this word'Again' might be the correct word here. Please check.",
      replacements: [],
      sentence: 'again',
      color: null,
      rule: 'KEYWORD',
      next: 0,
    },
    {
      value: 'is',
      order: 3,
      feedback: 'Did you mean "am"?',
      shortMessage: 'Possible agreement error',
      replacements: ['am'],
      offset: 15,
      length: 2,
      sentence: 'I is prefer the color read and white.',
      color: '#eeacff',
      rule: 'GRAMMAR',
      next: 2,
    },
    {
      value: 'is',
      order: 13,
      feedback: 'voce quis dizer "am"?',
      shortMessage: 'Possible error ',
      replacements: ['am'],
      offset: 66,
      length: 2,
      sentence: 'I is prefer the color read and white.',
      color: '#eeacff',
      rule: 'GRAMMAR',
      next: 2,
    },
    {
      value: 'prefer',
      order: 4,
      feedback: 'Did you mean "preferred" or "preferring"?',
      shortMessage: 'Possible grammar error',
      replacements: ['preferred', 'preferring'],
      offset: 18,
      length: 6,
      sentence: 'I is prefer the color read and white.',
      color: '#e9b271',
      rule: 'SPELLING',
      next: 3,
    },
    {
      value: 'read',
      order: 7,
      feedback:
        "Statistics suggests that 'red' (a color) might be the correct word here, not 'read' (something that is read). Please check. not 'read' (something that not 'read' (something thatnot 'read' (something thatnot 'read' (something that",
      shortMessage: '',
      replacements: ['red'],
      offset: 35,
      length: 4,
      sentence: 'I is prefer the color read and white.',
      color: '#e9b271',
      rule: 'SPELLING',
      next: 3,
    },
    {
      value: 'Seee',
      order: 10,
      feedback: 'Possible spelling mistake found',
      shortMessage: 'Spelling mistake',
      replacements: [
        'See',
        'Seen',
        'Sees',
        'Seed',
        'Seek',
        'Seem',
        'IEEE',
        'Seer',
        'Seep',
        'Sere',
        'DEEE',
        'EEE',
        'SEDEE',
        'WEEE',
      ],
      offset: 51,
      length: 4,
      sentence: 'Seee you.',
      color: '#e9b271',
      rule: 'SPELLING',
      next: 3,
    },
  ];
}
