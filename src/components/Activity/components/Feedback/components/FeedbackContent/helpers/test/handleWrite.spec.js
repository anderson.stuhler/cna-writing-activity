import * as handle from '../handleWrite';
import getItems from './mock/itemsToCarousel';

const tabs = [
  {
    value: 'KEYWORD',
    amount: 1,
    color: 'transparent',
    order: 0,
  },
  {
    value: 'VOCABULARY',
    amount: 1,
    color: '#e6a69d',
    order: 1,
  },
  {
    value: 'GRAMMAR',
    amount: 2,
    color: '#eeacff',
    order: 2,
  },
  {
    value: 'SPELLING',
    amount: 3,
    color: '#e9b271',
    order: 3,
  },
];

describe('Helper responsavel por manipular objetos da atividade write', () => {
  test('Deve retornar a ordem das tabs', () => {
    const KEYWORD = handle.tabsOrderList('KEYWORD');
    const VOCABULARY = handle.tabsOrderList('VOCABULARY');
    const GRAMMAR = handle.tabsOrderList('GRAMMAR');
    const SPELLING = handle.tabsOrderList('SPELLING');
    expect(KEYWORD).toBe(0);
    expect(VOCABULARY).toBe(1);
    expect(GRAMMAR).toBe(2);
    expect(SPELLING).toBe(3);
  });
  test('Deve retornar os items para o carrosel', () => {
    const items = handle.handleItemstoCarousel(getItems(), tabs).every(it => it.next !== null);
    expect(items).toBeTruthy();
  });
  test('Deve retornar as tabs', () => {
    const objTabs = handle.createTabs({}, 'SPELLING', getItems());
    expect(objTabs).toMatchObject({
      value: 'SPELLING',
      amount: 1,
      color: '#e9b271',
      order: 3,
    });
  });

  test('Deve retornar um objeto de tabs ', () => {
    const tabItems = handle.handleTabs(getItems());
    expect(tabItems).toMatchObject([
      {
        value: 'KEYWORD',
        amount: 1,
        color: 'transparent',
        order: 0,
      },
      {
        value: 'GRAMMAR',
        amount: 2,
        color: '#eeacff',
        order: 2,
      },
      {
        value: 'SPELLING',
        amount: 3,
        color: '#e9b271',
        order: 3,
      },
    ]);
  });
  test('Deve retornar o objeto sem os seus prototipos', () => {
    const obj = {
      teste: 'teste',
      fn: jest.fn(),
    };
    obj.fn.prototype.mock = () => 'teste';
    expect(obj.fn.prototype.mock()).toBe('teste');
    const newObj = handle.cloneDeep(obj);
    expect(newObj.fn).toBeUndefined();
  });
  test('Deve retornar a palavra com base no offset ', () => {
    const text = handle.findTextByOffset('i want a coffee', { offset: 9, length: 6 });
    expect(text).toEqual({
      length: 6,
      offset: 9,
      order: 9,
      replacements: [],
      value: 'coffee',
    });
  });
  test('Deve retornar texto com suas propriedades ', () => {
    const text = handle.handleText(
      'God evening. I is prefer the color read and white. Seee you again is',
      [
        {
          feedback: 'voce quis dizer "Good"?',
          shortMessage: 'Possible error ',
          replacements: [
            {
              value: 'Good',
            },
          ],
          offset: 0,
          length: 3,
          sentence: 'God evening',
          color: '#eeacff',
          rule: 'GRAMMAR',
        },
      ],
    );
    expect(text).toMatchObject([
      {
        feedback: 'voce quis dizer "Good"?',
        shortMessage: 'Possible error ',
        replacements: ['Good'],
        offset: 0,
        length: 3,
        sentence: 'God evening',
        color: '#eeacff',
        rule: 'GRAMMAR',
        order: 0,
        value: 'God',
      },
      {
        offset: 4,
        value: 'evening. I is prefer the color read and white. Seee you again is',
        order: 4,
      },
    ]);
  });
});
