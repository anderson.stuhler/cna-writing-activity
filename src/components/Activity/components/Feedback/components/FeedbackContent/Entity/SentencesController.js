import { handleText, handleTabs } from '../helpers/handleWrite';

export default class SentencesController {
  constructor(text, matches) {
    this.text = text || '';
    this.matches = matches || [];
  }

  createText() {
    return handleText(this.text, this.matches);
  }

  indexOfActiveWord(selected = {}) {
    return this.createText().findIndex(item => item.order === selected.order);
  }

  createTabs() {
    return handleTabs(this.matches);
  }
}
