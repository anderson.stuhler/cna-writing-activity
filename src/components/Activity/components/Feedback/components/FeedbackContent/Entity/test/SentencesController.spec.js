import SentencesController from '../SentencesController';
import FeedbackMock from '@/Mocks/WritingActivityFeedback.json';

const wrapper = new SentencesController(FeedbackMock.sentence, FeedbackMock.matches);

describe('Entity to create the sentences', () => {
  test('Should return the text with your colors', () => {
    const firstWord = wrapper.createText();
    expect(firstWord[0]).toEqual({
      message:
        '<div><span style="background-color: rgb(239, 246, 252); color: rgba(0, 0, 0, 0.9);">The word/sentence \'thank you\' should be used in your text. Use only one of them.</span></div>',
      shortMessage: null,
      offset: null,
      length: null,
      sentence: 'thank you',
      rule: 'KEYWORD',
      color: null,
      replacements: null,
      order: -1,
      value: 'thank you',
    });
  });
  test('Should return the indexOf param', () => {
    const index = wrapper.indexOfActiveWord({ order: 21 });
    expect(index).toEqual(6);
  });
  test('Should return the tabs', () => {
    const tabs = [
      {
        value: 'KEYWORD', amount: 2, color: 'transparent', order: 0,
      },
      {
        value: 'VOCABULARY', amount: 1, color: '#e6a69d', order: 1,
      },
      {
        value: 'GRAMMAR', amount: 1, color: '#eeacff', order: 2,
      },
      {
        value: 'SPELLING', amount: 1, color: '#e9b271', order: 3,
      },
    ];
    expect(tabs).toEqual(wrapper.createTabs());
  });
});
