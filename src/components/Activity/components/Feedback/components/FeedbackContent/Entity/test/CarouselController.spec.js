import CarouselController from '../CarouselController';

// Métodos estaticos ja foram testados no componente
const wrapper = new CarouselController();

describe('Entity to create a carousel controller ', () => {
  test('should increment the carousel', () => {
    wrapper.index = 0;
    wrapper.increment(5);
    expect(wrapper.index).toEqual(1);
  });
  test('should decrement the carousel', () => {
    wrapper.index = 4;
    wrapper.decrement(5);
    expect(wrapper.index).toEqual(3);
  });
  test('Should set the index', () => {
    wrapper.setIndex(2);
    expect(wrapper.index).toEqual(2);
  });
});
