import { handleItemstoCarousel } from '../helpers/handleWrite';

export default class CarouselController {
  constructor() {
    this.index = 0;
  }

  increment(total) {
    if (this.index < total - 1) {
      this.index += 1;
      return;
    }
    this.index = 0;
  }

  decrement(total) {
    if (this.index > 0) {
      this.index -= 1;
      return;
    }
    this.index = total - 1;
  }

  setIndex(index) {
    this.index = index;
  }

  static itemsToCarousel(matches, tabs) {
    return handleItemstoCarousel(matches.filter(it => it.rule), tabs);
  }

  static getFirstItemToCarousel(matches) {
    return matches.some(it => it.rule === 'KEYWORD')
      ? matches.filter(it => it.rule === 'KEYWORD')
      : matches.filter(it => it.rule);
  }

  static itemSelectedCarousel(itemSelected = {}, matches = []) {
    return itemSelected && itemSelected.rule
      ? itemSelected
      : this.getFirstItemToCarousel(matches)[0];
  }
}
