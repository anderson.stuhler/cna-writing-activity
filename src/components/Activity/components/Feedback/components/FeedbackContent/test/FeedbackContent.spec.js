import { shallowMount } from '@vue/test-utils';
import stubs from '@/tests/unit-config/vuetifyStubs';
import FeedbackActivityMock from './mock/mockFeedbackToTest.json';
import FeedbackContent from '../Index';
import SentencesController from '../Entity/SentencesController';

const wrapper = shallowMount(FeedbackContent, {
  stubs,
  propsData: { feedback: FeedbackActivityMock },
});

describe('Component to show feedback', () => {
  test('should increment caroulsel', () => {
    const spy = jest.spyOn(wrapper.vm.carousel, 'increment');
    wrapper.vm.increment();
    expect(spy).toHaveBeenCalled();
  });
  test('should decrement caroulsel', () => {
    const spy = jest.spyOn(wrapper.vm.carousel, 'decrement');
    wrapper.vm.decrement();
    expect(spy).toHaveBeenCalled();
  });
  test('Should select tab', () => {
    const spy = jest.spyOn(wrapper.vm, 'setSelected');
    wrapper.vm.selectTab('KEYWORD');
    expect(spy).toHaveBeenCalled();
  });
  test('Should select words', () => {
    const spy = jest.spyOn(wrapper.vm, 'selectWord');
    wrapper.vm.selectWord('test');
    expect(spy).toHaveBeenCalled();
  });
  test('Should set index in caroulsel entity', () => {
    const spy = jest.spyOn(wrapper.vm.carousel, 'setIndex');
    wrapper.vm.setSelected();
    expect(spy).toHaveBeenCalled();
  });
  test('Should be return a instance of Sentence', () => {
    expect(wrapper.vm.sentences).toBeInstanceOf(SentencesController);
  });
  test('Should return the selected item', () => {
    wrapper.vm.setSelected(2);
    expect(wrapper.vm.itemSelected.rule).toEqual('VOCABULARY');
  });
  test('Should create the tabs', () => {
    const [tabs] = wrapper.vm.tabs;
    expect(tabs).toMatchObject({
      value: 'KEYWORD',
      amount: 2,
      color: 'transparent',
      order: 0,
    });
  });
  test('Should create the object that makes the words and your colors ', () => {
    const words = wrapper.vm.sentecesWithColors[0];
    const obj = {
      message:
        '<div><span style="background-color: rgb(239, 246, 252); color: rgba(0, 0, 0, 0.9);">The word/sentence \'thank you\' should be used in your text. Use only one of them.</span></div>',
      shortMessage: null,
      offset: null,
      length: null,
      sentence: 'thank you',
      rule: 'KEYWORD',
      color: null,
      replacements: null,
      order: -1,
      value: 'thank you',
    };
    expect(words).toEqual(obj);
  });
  test('Should return the index of active word', () => {
    wrapper.vm.setSelected(0);
    expect(wrapper.vm.indexOfActiveWord).toEqual(0);
  });
});
