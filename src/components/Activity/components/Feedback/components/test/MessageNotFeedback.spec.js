import { shallowMount } from '@vue/test-utils';
import MessageNotFeedback from '../MessageNotFeedback';

const wrapper = shallowMount(MessageNotFeedback);

describe('component to display the message if there is no feedback', () => {
  test('Shoud exist', () => {
    expect(wrapper.isVueInstance).toBeTruthy();
  });
});
