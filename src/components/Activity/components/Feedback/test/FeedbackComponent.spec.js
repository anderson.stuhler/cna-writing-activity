import { shallowMount } from '@vue/test-utils';
import Feedback from '../Index';
import mockActivityFeedback from '@/Mocks/WritingActivityFeedback.json';

const wrapper = shallowMount(Feedback, {
  propsData: {
    feedback: mockActivityFeedback,
  },
  mocks: {
    $vuetify: { breakpoint: {} },
  },
});

describe('Component that makes feedback ', () => {
  test('Should return if exist any feedback', () => {
    expect(wrapper.vm.hasMatches).toBeTruthy();
  });
});
