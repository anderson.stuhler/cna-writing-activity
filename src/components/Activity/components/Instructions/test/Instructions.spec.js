import { shallowMount } from '@vue/test-utils';
import stubs from '@/tests/unit-config/vuetifyStubs';
import Instructions from '../Index';

const wrapper = shallowMount(Instructions, {
  stubs,
  propsData: {
    questions: [],
  },
  mocks: {
    $vuetify: {
      breakpoint: {},
    },
  },
});

describe('component to show the Instructions of activity', () => {
  test('Should set the active tab ', () => {
    wrapper.vm.selectedItem(1);
    expect(wrapper.vm.activeBottomItem).toEqual(1);
  });
});
