import { shallowMount } from '@vue/test-utils';

import WritingInstructionsText from '../Index';

const wrapper = shallowMount(WritingInstructionsText);

describe('component to show the instructions in text format', () => {
  test('Should exist ', () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
  });
});
