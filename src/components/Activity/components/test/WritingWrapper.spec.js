import { shallowMount } from '@vue/test-utils';
import stubs from '@/tests/unit-config/vuetifyStubs';
import WritingWrapper from '../WritingWrapper';
import Activity from '../../../../Entity/Activity/Activity';

const wrapper = shallowMount(WritingWrapper, {
  stubs,
  propsData: {
    activity: new Activity(),
  },
  mocks: {
    $vuetify: {
      breakpoint: {},
    },
  },
});

describe('component Wrapper', () => {
  test('Should set as active the tab ', () => {
    wrapper.vm.setTab(1);
    expect(wrapper.emitted()).toHaveProperty('setTab');
  });
});
