import { shallowMount, createLocalVue } from '@vue/test-utils';
import Activity from '../Index';

const localVue = createLocalVue();

localVue.directive('touch', jest.fn());

const wrapper = shallowMount(Activity, { localVue });

describe('Component to create the activity', () => {
  test('Should emmit the reaction action', () => {
    wrapper.vm.reaction();
    expect(wrapper.emitted()).toHaveProperty('reaction');
  });
  test('Should emmit the feedbackContinue action', () => {
    wrapper.vm.feedbackContinue();
    expect(wrapper.emitted()).toHaveProperty('feedbackContinue');
  });
  test('Should emmit the exit action', () => {
    wrapper.vm.exit();
    expect(wrapper.emitted()).toHaveProperty('exit');
  });
  test('Should emmit the check action', () => {
    wrapper.vm.check('teste');
    expect(wrapper.vm.activeTab).toEqual(2);
    expect(wrapper.emitted()).toHaveProperty('check');
  });
  test('Should emmit the help action', () => {
    wrapper.vm.help();
    expect(wrapper.emitted()).toHaveProperty('help');
  });
  test('Should set the tab that is active', () => {
    wrapper.vm.setTab(1);
    expect(wrapper.vm.activeTab).toEqual(1);
  });
  test('Should set next tab', () => {
    wrapper.vm.setTab(1);
    wrapper.vm.next();
    expect(wrapper.vm.activeTab).toEqual(2);
    expect(wrapper.vm.slideName).toEqual('slide-left');
  });
  test('Should set back tab', () => {
    wrapper.vm.setTab(2);
    wrapper.vm.back();
    expect(wrapper.vm.activeTab).toEqual(1);
    expect(wrapper.vm.slideName).toEqual('slide-right');
  });
});
