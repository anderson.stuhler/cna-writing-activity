import { shallowMount } from '@vue/test-utils';
import Mobile from '../Index';
import mockActivity from '@/Mocks/WritingActivityMock.json';

const wrapper = shallowMount(Mobile, {
  propsData: {
    activity: mockActivity,
  },
  mocks: {
    $vuetify: { breakpoint: {} },
  },
});
describe('Component that makes the wrapper for mobile', () => {
  test('Should exist', () => {
    expect(wrapper.isVueInstance).toBeTruthy();
  });
});
