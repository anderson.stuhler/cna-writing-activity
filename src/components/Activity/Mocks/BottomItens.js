const bottomIcons = [
  { type: 'text', description: 'Read text', icon: 'text_format' },
  { type: 'image', description: 'See model', icon: 'image' },
  { type: 'audio', description: 'Listen dialog', icon: 'volume_up' },
  { type: 'video', description: 'See video', icon: 'videocam' },
];

export default bottomIcons;
