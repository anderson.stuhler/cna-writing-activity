const tabs = [
  {
    order: 0,
    description: 'Instructions',
    active: true,
  },
  {
    order: 1,
    description: 'Your text',
    active: false,
  },
  {
    order: 2,
    description: 'Feedback',
    active: false,
  },
];

export default tabs;
