import { shallowMount } from '@vue/test-utils';
import Desktop from '../Index';
import mockActivity from '@/Mocks/WritingActivityMock.json';

const wrapper = shallowMount(Desktop, {
  propsData: {
    activity: mockActivity,
  },
  mocks: {
    $vuetify: { breakpoint: {} },
  },
});
describe('Component that makes the wrapper for Desktop', () => {
  test('Should exist', () => {
    expect(wrapper.isVueInstance).toBeTruthy();
  });
});
