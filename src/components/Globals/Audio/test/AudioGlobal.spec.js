import { shallowMount } from '@vue/test-utils';
import stubs from '@/tests/unit-config/vuetifyStubs';
import AudioGlobal from '../Index';

const wrapper = shallowMount(AudioGlobal, {
  stubs,

  propsData: {
    src: 'teste',
  },
});

describe('Component to play audio', () => {
  test('should mute/unmute the audio', () => {
    wrapper.vm.$refs = {
      player: {
        muted: false,
      },
    };
    wrapper.vm.muted = true;
    wrapper.vm.mute();
    expect(wrapper.vm.muted).toBeFalsy();
    wrapper.vm.mute();
    expect(wrapper.vm.muted).toBeTruthy();
  });
  test('Should emmit when finish audio', () => {
    wrapper.vm.playing = true;
    wrapper.vm.progressStatus = 10;
    wrapper.vm.onAudioEnd();
    expect(wrapper.vm.playing).toBeFalsy();
    expect(wrapper.vm.progressStatus).toEqual(100);
    expect(wrapper.emitted()).toHaveProperty('playing-change');
  });
});
