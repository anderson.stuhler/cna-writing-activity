const AudioHelper = {
  convertTimeHHMMSS(val) {
    const hhmmss = new Date(Number(val) * 1000).toISOString().substr(11, 8);
    return hhmmss.indexOf('00:') === 0 ? hhmmss.substr(3) : hhmmss;
  },
  getAudioIcon(recording, playing) {
    const icon = {
      play_arrow: !recording && !playing,
      pause: !recording && playing,
      block: recording === true,
    };
    return Object.keys(icon).find(it => icon[it]);
  },
};
export default AudioHelper;
