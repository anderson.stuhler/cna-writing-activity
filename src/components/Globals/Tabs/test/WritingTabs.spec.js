import { shallowMount } from '@vue/test-utils';
import stubs from '@/tests/unit-config/vuetifyStubs';
import WritingTabs from '../Index';

const wrapper = shallowMount(WritingTabs, { stubs });

describe('component tabs', () => {
  test('Should set as active the tab ', () => {
    wrapper.vm.setTab(1);
    expect(wrapper.emitted()).toHaveProperty('setTab');
  });
});
