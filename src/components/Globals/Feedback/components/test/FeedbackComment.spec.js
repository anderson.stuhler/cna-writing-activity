import { shallowMount } from '@vue/test-utils';
import stubs from '@/tests/unit-config/vuetifyStubs';
import FeedbackComment from '../FeedbackComment';

const wrapper = shallowMount(FeedbackComment, { stubs });

describe('Component to comment in activity', () => {
  test('should emmit a comment', () => {
    wrapper.vm.commentAction();
    expect(wrapper.emitted()).toHaveProperty('input');
  });
});
