import { shallowMount } from '@vue/test-utils';
import stubs from '@/tests/unit-config/vuetifyStubs';
import FeedbackInfo from '../FeedbackInfo';

const wrapper = shallowMount(FeedbackInfo, { stubs });

describe('Component to comment in activity', () => {
  test('should emmit a commemt', () => {
    wrapper.vm.translated = false;
    wrapper.vm.toggleTranslated();
    expect(wrapper.vm.translated).toBeTruthy();
  });
  test('Should return the title if correct or not', () => {
    wrapper.setProps({ feedback: { correct: true } });
    expect(wrapper.vm.title).toEqual("That's Correct!");
    wrapper.setProps({ feedback: { correct: false } });
    expect(wrapper.vm.title).toEqual('Uh-oh!');
  });
  test('should return the feedback translated or original', () => {
    wrapper.setProps({ feedback: { id: true, text: { translation: 'teste', value: 'test' } } });
    wrapper.vm.translated = true;
    expect(wrapper.vm.feedbackText).toEqual('teste');
    wrapper.vm.translated = false;
    expect(wrapper.vm.feedbackText).toEqual('test');
  });
  test('Should return the label translated', () => {
    wrapper.vm.translated = true;
    expect(wrapper.vm.translatedLabel).toEqual('Show Original');
    wrapper.vm.translated = false;
    expect(wrapper.vm.translatedLabel).toEqual('Translate');
  });
});
