import { shallowMount } from '@vue/test-utils';
import stubs from '@/tests/unit-config/vuetifyStubs';
import FeedbackThumbs from '../FeedbackThumbs';

const wrapper = shallowMount(FeedbackThumbs, { stubs });

describe('Component to evaluate the activity', () => {
  test('should emmit a thumb of activity', () => {
    wrapper.vm.setReaction('just a test');
    expect(wrapper.vm.reaction).toEqual('just a test');
    expect(wrapper.emitted()).toHaveProperty('input');
  });
  test('Should return the icon color', () => {
    wrapper.vm.reaction = 'POSITIVE';
    let color = wrapper.vm.likeIconColor;
    expect(color).toEqual('green');
    wrapper.vm.reaction = 'NEGATIVE';
    color = wrapper.vm.deslikeIconColor;
    expect(color).toEqual('gray');
  });
});
