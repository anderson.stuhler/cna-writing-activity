import { shallowMount } from '@vue/test-utils';
import stubs from '@/tests/unit-config/vuetifyStubs';
import Thumbs from '../Thumbs';

const wrapper = shallowMount(Thumbs, { stubs });

describe('Component to icon like/deslike', () => {
  test('should exist', () => {
    expect(wrapper.isVueInstance).toBeTruthy();
  });
});
