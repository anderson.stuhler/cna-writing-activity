import { shallowMount } from '@vue/test-utils';
import stubs from '@/tests/unit-config/vuetifyStubs';
import FeedbackToggle from '../FeedbackToggle';

const wrapper = shallowMount(FeedbackToggle, { stubs });

describe('Component to toggle the feedback', () => {
  test('should emmit the status of active active or not', () => {
    wrapper.setProps({ value: true });
    wrapper.vm.toggle();
    expect(wrapper.emitted().input[0][0]).toEqual(false);
    wrapper.setProps({ value: false });
    wrapper.vm.toggle();
    expect(wrapper.emitted().input[1][0]).toEqual(true);
  });
  test('Should return the arrow icon', () => {
    wrapper.setProps({ value: false });
    expect(wrapper.vm.arrowIcon).toEqual('keyboard_arrow_up');
    wrapper.setProps({ value: true });
    expect(wrapper.vm.arrowIcon).toEqual('keyboard_arrow_down');
  });
});
