import { shallowMount } from '@vue/test-utils';
import stubs from '@/tests/unit-config/vuetifyStubs';
import Feedback from '../Index';

const wrapper = shallowMount(Feedback, {
  stubs,
  propsData: {
    correct: false,
  },
});

describe('Component to create a Feedback', () => {
  test('Should be emmit a reaction', () => {
    wrapper.vm.commentAction();
    expect(wrapper.emitted()).toHaveProperty('comment');
  });
  test('Should be emmit a reaction', () => {
    wrapper.vm.reaction();
    expect(wrapper.emitted()).toHaveProperty('reaction');
  });
  test('Should be emmit a continue button', () => {
    wrapper.vm.feedbackContinue();
    expect(wrapper.emitted()).toHaveProperty('feedbackContinue');
  });
  test('Should return a style of feedback corrrect', () => {
    wrapper.setProps({ feedback: { correct: true } });
    const style = wrapper.vm.styleFeedback;
    const feedback = {
      feedback__correct: true,
      feedback__wrong: false,
      feedback__active: undefined,
      feedback__toggle: false,
    };
    expect(style).toMatchObject(feedback);
  });
  test('Should return a style of feedback not corrrect', () => {
    wrapper.setProps({ feedback: { correct: false } });
    const style = wrapper.vm.styleFeedback;
    const feedback = {
      feedback__correct: false,
      feedback__wrong: true,
      feedback__active: undefined,
      feedback__toggle: false,
    };
    expect(style).toMatchObject(feedback);
  });
  test('Should return a style of feedback active', () => {
    wrapper.setProps({ feedback: { correct: false, id: 'teste' } });
    const style = wrapper.vm.styleFeedback;
    const feedback = {
      feedback__correct: false,
      feedback__wrong: true,
      feedback__active: 'teste',
      feedback__toggle: false,
    };
    expect(style).toMatchObject(feedback);
  });
  test('Should return a style of feedback toggle', () => {
    wrapper.vm.open = false;
    const style = wrapper.vm.styleFeedback;
    const feedback = {
      feedback__correct: false,
      feedback__wrong: true,
      feedback__active: 'teste',
      feedback__toggle: true,
    };
    expect(style).toMatchObject(feedback);
  });
});
