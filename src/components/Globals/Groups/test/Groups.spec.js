import { shallowMount } from '@vue/test-utils';
import stubs from '@/tests/unit-config/vuetifyStubs';
import Groups from '../Index';

const wrapper = shallowMount(Groups, {
  stubs,
  propsData: {
    numberItems: 10,
    activeItem: 0,
  },
});

describe('Component to create a Feedback', () => {
  test('Should be emmit a next number', () => {
    wrapper.vm.next();
    expect(wrapper.emitted().next[0][0]).toEqual(1);
  });
  test('Should be emmit a next number but must return to zero', () => {
    wrapper.setProps({ activeItem: 9 });
    wrapper.vm.next();
    expect(wrapper.emitted().next[1][0]).toEqual(0);
  });
  test('Should be emmit a back number', () => {
    wrapper.setProps({ activeItem: 5 });
    wrapper.vm.back();
    expect(wrapper.emitted().back[0][0]).toEqual(4);
  });
  test('Should be emmit a back number but must return the max number', () => {
    wrapper.setProps({ activeItem: 0, numberItems: 10 });
    wrapper.vm.back();
    expect(wrapper.emitted().back[1][0]).toEqual(9);
  });
});
