import { shallowMount } from '@vue/test-utils';
import Video from '../Index';

const wrapper = shallowMount(Video, {
  mocks: {
    $vuetify: {
      breakpoint: {
        mdAndDown: true,
      },
    },
  },
  propsData: {
    media: 'URLTest',
    index: 0,
  },
});

describe('Component to show the video', () => {
  test('Should return the URL of media ', () => {
    expect(wrapper.vm.videoId).toEqual('URLTest');
  });
  test('Should return width of component', () => {
    expect(wrapper.vm.width).toEqual('500');
  });
  test('Should return height of component', () => {
    expect(wrapper.vm.height).toEqual('400');
  });
});
