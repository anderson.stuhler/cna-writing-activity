import * as Mutations from '../../mutations';

export default {
  setAuthToken({ commit }, token) {
    commit(Mutations.SET_AUTH_TOKEN, token);
  },
  removeAuthToken({ commit }) {
    commit(Mutations.SET_AUTH_TOKEN, null);
  },
  setRealmAccess({ commit }, realmAccess) {
    commit(Mutations.SET_REALM_ACCESS, realmAccess);
  },
  removeRealmAccess({ commit }) {
    commit(Mutations.SET_REALM_ACCESS, null);
  },
  setAuthenticated({ commit }, authenticated) {
    commit(Mutations.SET_AUTHENTICATED, authenticated);
  },
  removeAuthenticated({ commit }) {
    commit(Mutations.SET_AUTHENTICATED, null);
  },
};
