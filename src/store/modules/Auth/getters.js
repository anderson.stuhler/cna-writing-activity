export default {
  token: store => store.token,
  realmAccess: store => store.realmAccess,
  authenticated: store => store.authenticated,
};
