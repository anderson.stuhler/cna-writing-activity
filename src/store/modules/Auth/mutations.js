import * as Mutations from '../../mutations';

export default {
  [Mutations.SET_AUTH_TOKEN](store, token) {
    store.token = token;
  },
  [Mutations.SET_REALM_ACCESS](store, realmAccess) {
    store.realmAccess = realmAccess;
  },
  [Mutations.SET_AUTHENTICATED](store, authenticated) {
    store.authenticated = authenticated;
  },
};
