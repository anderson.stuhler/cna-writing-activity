export const SET_AUTH_TOKEN = 'setAuthToken';
export const SET_REALM_ACCESS = 'setRealmAccess';
export const SET_AUTHENTICATED = 'setAuthenticated';
