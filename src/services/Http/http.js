import axios from 'axios';
import Keycloak from '../Keycloak/Keycloak';
// import store from '../../store/index';

export default class HttpBuilder {
  constructor() {
    this.baseUrl = '/api';
    this.instance = axios.create({
      baseURL: this.baseUrl,
    });

    this.instance.interceptors.request.use((config) => {
      this.keycloak = Keycloak.getInstance();
      config.headers.Authorization = `Bearer ${this.keycloak.token}`;
      return config;
    });

    this.instance.interceptors.response.use(
      response => response,
      (error) => {
        const { response } = error;
        if (response && response.status === 401) {
          this.keycloak = Keycloak.getInstance();
          return new Promise((resolve, reject) => {
            this.keycloak
              .updateToken()
              .success((refreshed) => {
                if (refreshed) {
                  response.config.headers.Authorization = `Bearer ${this.keycloak.token}`;
                  response.config.url = response.config.url.replace(this.baseUrl, '');
                  resolve(this.instance(response.config));
                }
              })
              .error((err) => {
                this.keycloak.login();
                reject(err);
              });
          });
        }
        return Promise.reject(error);
      },
    );

    return this.instance;
  }
}
