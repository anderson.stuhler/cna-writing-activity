import Vue from 'vue';

export default {
  getInstance() {
    return Vue.prototype.$keycloak;
  },
};
