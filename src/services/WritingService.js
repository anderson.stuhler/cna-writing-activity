import HttpBuilder from './Http/http';

export default class WritingService {
  constructor(Http = HttpBuilder) {
    this.http = new Http();
  }

  getAlgumaCoisa(id = 'sadas ad') {
    return this.http.get(`/writing/${id}`);
  }

  getActivityById(writingId) {
    return this.http.get(`writing/${writingId}`);
  }

  getFeedbackByWritingId(writingId) {
    return this.http.get(`writing/${writingId}/feedback/student`);
  }

  getActivityBySequence(sequenceId) {
    return this.http.get(`writing/sequence/${sequenceId}`);
  }

  checkAnswer(payload) {
    return this.http.post('writing/answer-writing/', payload);
  }

  previewAnswer(payload) {
    return this.http.post('/writing/validate/preview', payload);
  }

  getProgress(writingId) {
    return this.http.get(`/writing/${writingId}/progress/student`);
  }

  getLifes(id) {
    return this.http.get(`/writing/lifes/${id}`);
  }

  getStep(id) {
    return this.http.get(`/writing/steps/${id}`);
  }
}
