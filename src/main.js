import Vue from 'vue';
import './plugins/Vuetify/vuetify';
import App from './App.vue';
import 'vuetify/dist/vuetify.min.css';
import './styles/global.sass';
import keycloakConfig from './keycloak.config.js';
import store from './store/index';

const render = keycloakConfig();

const init = () => {
  Vue.config.productionTip = false;

  new Vue({
    store,
    render: h => h(App),
  }).$mount('#app');
};

render
  .then(() => {
    init();
  })
  .catch(() => {
    init();
  });
