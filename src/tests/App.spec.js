import { shallowMount } from '@vue/test-utils';
import App from '../App';
import stubs from './unit-config/vuetifyStubs';

const wrapper = shallowMount(App, {
  stubs,
  mocks: {
    $vuetify: {
      breakpoint: {},
    },
  },
});

describe('CNA-ACTIVITY-WRITING - Component', () => {
  test('Should emmit help', () => {
    wrapper.vm.check();
    expect(wrapper.emitted()).toHaveProperty('check');
  });
  test('Should emmit help', () => {
    wrapper.vm.help();
    expect(wrapper.emitted()).toHaveProperty('help');
  });
  test('Should emmit exit', () => {
    wrapper.vm.exit();
    expect(wrapper.emitted()).toHaveProperty('exit');
  });
  test('Should emmit feedbackContinue', () => {
    wrapper.vm.feedbackContinue();
    expect(wrapper.emitted()).toHaveProperty('feedbackContinue');
  });
  test('Should emmit reaction', () => {
    wrapper.vm.reaction();
    expect(wrapper.emitted()).toHaveProperty('evaluate');
  });
});
