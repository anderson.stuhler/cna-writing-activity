const stubs = {
  'v-card': '</div>',
  'v-layout': '</div>',
  'v-spacer': '</div>',
  'v-app': '</div>',
  'v-list-tile-action': '</div>',
  'v-list-tile-title': '</div>',
  'v-list-tile-sub-title': '</div>',
  'v-list-tile-content': '</div>',
  'v-divider': '</div>',
  'v-list-tile': '</div>',
};
export default stubs;
