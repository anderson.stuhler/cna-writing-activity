import WritingService from '@/services/WritingService';
import Activity from '../../Entity/Activity/Activity';
import Feedback from '../../Entity/Feedback/Feedback';
import Payload from '../../Entity/Activity/Payload';
import Preview from '../../Entity/Preview/Preview';

export default class ActivityFactory {
  constructor(props, httpService) {
    this.writingId = props.writingId || '';
    this.sequenceId = props.sequenceId || '';
    this.activityReceived = props.activityReceived || {};
    this.feedbackReceived = props.feedbackReceived || {};
    this.writingService = new WritingService(httpService);
  }

  getProgress(writingId) {
    return new Promise((resolve, reject) => {
      this.writingService
        .getProgress(writingId)
        .then(({ data }) => {
          resolve(data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  getActivity() {
    return new Promise((resolve, reject) => {
      if (this.writingId) {
        resolve(this.getActivityById(this.writingId));
      } else if (this.sequenceId) {
        resolve(this.getActivityBySequence(this.sequenceId));
      } else if (this.hasActivityBySequence()) {
        resolve(new Activity(this.activityReceived));
      } else {
        reject(
          new Error('É necessário o id da atividade ou da senquence para buscar uma atividade'),
        );
      }
    });
  }

  hasActivityBySequence() {
    return this.activityReceived.question
      && this.activityReceived.question.length > 0;
  }

  getActivityBySequence() {
    return new Promise((resolve, reject) => {
      this.writingService
        .getActivityBySequence(this.sequenceId)
        .then(({ data }) => {
          resolve(new Activity(data));
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  getActivityById() {
    return new Promise((resolve, reject) => {
      this.writingService
        .getActivityById(this.writingId)
        .then(({ data }) => {
          resolve(new Activity(data));
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  getFeedbackByWritingId(id) {
    return new Promise((resolve, reject) => {
      if (this.feedbackReceived.id) {
        resolve(this.feedbackReceived);
      }
      this.writingService
        .getFeedbackByWritingId(id)
        .then(({ data }) => {
          resolve(new Feedback(data));
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  previewAnswer(text, activity) {
    return new Promise((resolve, reject) => {
      const payload = new Preview({
        writing: activity,
        text,
      });
      this.writingService
        .previewAnswer(payload)
        .then(({ data }) => {
          resolve(new Feedback(data));
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  checkAnswer(activityId, awnser) {
    return new Promise((resolve, reject) => {
      const payload = new Payload(activityId, awnser);
      this.writingService
        .checkAnswer(payload)
        .then(({ data }) => {
          resolve(new Feedback(data));
        })
        .catch((err) => {
          reject(err);
        });
    });
  }
}
