import WritingActivity from './Activity/Index';
import './plugins/Vuetify/vuetify';
import 'vuetify/dist/vuetify.min.css';
import './styles/global.sass';

export default WritingActivity;
