export default class Preview {
  constructor(data = {}) {
    this.writing = data.writing;
    this.text = data.text || '';
  }
}
