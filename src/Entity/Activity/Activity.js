import Description from './Description';
import Question from './Question';
import Senteces from './Sentences';

export default class Activity {
  constructor(data = {}) {
    this.id = data.id || data.temporaryUID;
    this.minWords = data.minWords || 5;
    this.maxWords = data.maxWords || 5;
    this.maxWordsMessage = `Wow! You have written enough. The number of words expected for this activity is about ${
      this.maxWords
    } words.`;
    this.minWordsMessage = `Write ${this.minWords} words to check your answer`;
    this.awnser = data.awnser || '';
    this.type = data.type || 'writing';
    this.description = new Description(data.description || {});
    this.$question = data.question || [];
    this.$sentences = data.sentences || [];
  }

  get question() {
    return this.$question.map(it => new Question(it));
  }

  get sentences() {
    return this.$sentences.map(it => new Senteces(it));
  }

  static getNumberOfWords(text) {
    return text
      .replace(/\n/g, ' ')
      .split(' ')
      .filter(it => it).length;
  }

  getTooltip(text = '') {
    const words = Activity.getNumberOfWords(text);
    const messages = {
      minWordsMessage: words < this.minWords,
      maxWordsMessage: words > this.maxWords,
    };
    const tooltip = Object.keys(messages).filter(it => messages[it]);
    return this[tooltip];
  }
}
