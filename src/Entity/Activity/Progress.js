export default class Progress {
  constructor(data = {}) {
    this.remainingLives = typeof data.remainingLives === 'number' ? data.remainingLives : 3;
    this.concluded = data.concluded || false;
    this.answerStep = data.answerStep || '';
  }

  get step() {
    if (this.answerStep === 'INITIAL_STEP' || this.answerStep === 'SECOND_ANSWER') {
      return 1;
    }
    return 2;
  }
}
