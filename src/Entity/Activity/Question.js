export default class Question {
  constructor(data = {}) {
    this.type = data.type || '';
    this.description = data.description || null;
    this.point = data.point || [];
    this.value = data.value || data.url;
  }
}
