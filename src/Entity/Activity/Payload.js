export default class Payload {
  constructor(id, answer) {
    this.writingId = id || undefined;
    this.text = answer || '';
  }
}
