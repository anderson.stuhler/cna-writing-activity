export default class Senteces {
  constructor(data = {}) {
    this.id = data.id || undefined;
    this.type = data.type || '';
    this.words = data.words || [];
    this.feedback = data.feedback || '';
  }
}
