import Question from '../Question';

const wrapper = new Question();

describe('Entity to create a Question of activity', () => {
  test('Should create a object with all atributes', () => {
    const attr = ['type', 'description', 'point', 'value'];
    const wrapperKeys = Object.keys(wrapper);
    expect(wrapperKeys).toMatchObject(attr);
  });
});
