import Sentences from '../Sentences';

const wrapper = new Sentences();

describe('Entity to create a Sentences of activity', () => {
  test('Should create a object with all atributes', () => {
    const attr = ['id', 'type', 'words', 'feedback'];
    const wrapperKeys = Object.keys(wrapper);
    expect(wrapperKeys).toMatchObject(attr);
  });
});
