import Description from '../Description';

const wrapper = new Description();

describe('Entity to create a Description of activity', () => {
  test('Should create a object with all atributes', () => {
    const attr = ['text', 'translation'];
    const wrapperKeys = Object.keys(wrapper);
    expect(wrapperKeys).toMatchObject(attr);
  });
});
