import Activity from '../Activity';
import Question from '../Question';
import Senteces from '../Sentences';

const wrapper = new Activity();

describe('Entity that create a instance of Activity Writing', () => {
  test('Should have all properties', () => {
    const properties = [
      'id',
      'acceptedPercent',
      'minWords',
      'maxWords',
      'maxWordsMessage',
      'minWordsMessage',
      'awnser',
      'skip',
      'type',
      'tip',
      'description',
      '$question',
      '$sentences',
    ];
    const wrapperKeys = Object.keys(wrapper);
    expect(wrapperKeys).toMatchObject(properties);
  });
  test('Should return an array with your items being an instance of Question', () => {
    wrapper.$question = [
      {
        type: '',
        description: '',
        point: '',
        value: '',
      },
    ];
    const [question] = wrapper.question;
    expect(question).toBeInstanceOf(Question);
  });
  test('Should return an array with your items being an instance of Sentence', () => {
    wrapper.$sentences = [
      {
        id: '',
        type: '',
        words: [],
        feedback: '',
      },
    ];
    const [sentences] = wrapper.sentences;
    expect(sentences).toBeInstanceOf(Senteces);
  });

  test('Should return a number of words in a sentence', () => {
    const words = Activity.getNumberOfWords('just a test');
    expect(words).toEqual(3);
  });
  test('Should return a tooltip', () => {
    const tooltipMinLetter = wrapper.getTooltip('just a test');
    expect(tooltipMinLetter).toEqual('Write 5 words to check your answer');
    const tooltipMaxLetter = wrapper.getTooltip('bla bla bla bla  bla bla bla bla bla');
    expect(tooltipMaxLetter).toEqual('Wow! You have written enough. The number of words expected for this activity is about 5 words.');
  });
});
