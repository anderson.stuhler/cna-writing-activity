export default class FeedbackText {
  constructor(data = {}) {
    this.value = data.text || data.value;
    this.translation = data.translation || '';
  }
}
