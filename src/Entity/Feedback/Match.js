import Replacements from './Replacements';

export default class Match {
  constructor(data = {}) {
    this.message = data.message;
    this.shortMessage = data.shortMessage;
    this.offset = data.offset;
    this.length = data.length;
    this.sentence = data.sentence;
    this.rule = data.rule;
    this.color = data.color;
    this.$replacements = data.replacements || [];
  }

  get replacements() {
    return this.$replacements.map(it => new Replacements(it));
  }
}
