import FeedbackText from '../FeedbackText';

const wrapper = new FeedbackText();

describe('Entity that create a instance of FeedbackText Writing', () => {
  test('Should have all properties', () => {
    const properties = ['value', 'translation'];
    const wrapperKeys = Object.keys(wrapper);
    expect(wrapperKeys).toMatchObject(properties);
  });
});
