import Feedback from '../Feedback';
import Match from '../Match';

const wrapper = new Feedback();

describe('Entity that create a instance of Feedback Writing', () => {
  test('Should have all properties', () => {
    const properties = [
      'id',
      'earnedPoints',
      'correct',
      'sentence',
      'additionalInfo',
      'text',
      '$matches',
    ];
    const wrapperKeys = Object.keys(wrapper);
    expect(wrapperKeys).toMatchObject(properties);
  });
  test('Should return an array with instances of match', () => {
    wrapper.$matches = [
      {
        message: '',
        shortMessage: '',
        offset: '',
        length: '',
        sentence: '',
        rule: '',
        color: '',
        $replacements: [],
      },
    ];
    const [matches] = wrapper.matches;
    expect(matches).toBeInstanceOf(Match);
  });
});
