import Match from '../Match';
import Replacements from '../Replacements';

const wrapper = new Match();

describe('Entity that create a instance of Match Writing', () => {
  test('Should have all properties', () => {
    const properties = [
      'message',
      'shortMessage',
      'offset',
      'length',
      'sentence',
      'rule',
      'color',
      '$replacements',
    ];
    const wrapperKeys = Object.keys(wrapper);
    expect(wrapperKeys).toMatchObject(properties);
  });
  test('Should return an array with instances of Replacements', () => {
    wrapper.$replacements = [
      {
        value: '',
      },
    ];
    const [replacements] = wrapper.replacements;
    expect(replacements).toBeInstanceOf(Replacements);
  });
});
