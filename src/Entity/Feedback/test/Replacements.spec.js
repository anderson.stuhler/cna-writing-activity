import Replacements from '../Replacements';

const wrapper = new Replacements();

describe('Entity that create a instance of Replacements Writing', () => {
  test('Should have all properties', () => {
    const properties = ['value'];
    const wrapperKeys = Object.keys(wrapper);
    expect(wrapperKeys).toMatchObject(properties);
  });
});
