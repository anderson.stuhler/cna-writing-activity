import FeedbackText from './FeedbackText';
import Match from './Match';
import Progress from '../Activity/Progress';

export default class Feedback {
  constructor(data = {}) {
    this.id = data.writingId || data.id;
    this.earnedPoints = data.earnedPoints || 2;
    this.correct = data.correct || false;
    this.sentence = data.sentence || '';
    this.additionalInfo = data.additionalInfo || null;
    this.text = new FeedbackText(data.feedback || data.text);
    this.progress = new Progress(data);
    this.answerStep = data.answerStep || '';
    this.$matches = data.matches || [];
    this.showFeedback = false;
  }

  get matches() {
    return this.$matches.map(it => new Match(it));
  }

  toggleFeedback(data) {
    this.showFeedback = data === true;
  }
}
