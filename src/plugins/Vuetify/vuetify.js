import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/src/stylus/app.styl';

Vue.use(Vuetify, {
  theme: {
    primary: '#2E2E78',
    gray: '#808080',
    'gray-btn': '#e6e6e6',
    grammar: '#eeacff',
    vocabulary: '#eeacff',
    spelling: '#e9b271',
    indigo: '#2d2e78',
    green: '#c7eb40',
    yellow: '#d6e702',
  },
  iconfont: 'md',
});
