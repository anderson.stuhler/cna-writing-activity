import * as keycloakJS from 'keycloak-js';
import Vue from 'vue';
import store from './store';

export default function keycloakConfig() {
  const options = {
    onLoad: 'check-sso',
    responseMode: 'query',
  };
  return new Promise((resolve, reject) => {
    const keycloak = keycloakJS({
      realm: 'cnago',
      url: process.env.VUE_APP_KEYCLOAK_URL,
      'ssl-required': 'external',
      resource: 'webapp',
      'public-client': true,
      clientId: 'webapp',
    });

    function setKeyclockToStore() {
      store.dispatch('setAuthToken', keycloak.token);
      store.dispatch('setRealmAccess', keycloak.realmAccess);
    }

    function updateWatchVariables(isAuthenticated = false) {
      if (isAuthenticated) {
        keycloak.onReady = (authenticated) => {
          if (!authenticated) {
            keycloak.login();
          }
        };
        keycloak.onAuthError = () => {
          keycloak.login();
        };
        setKeyclockToStore();
        store.dispatch('setAuthenticated', isAuthenticated);
        resolve();
      } else {
        reject();
      }
    }
    setInterval(() => {
      keycloak.updateToken(300000).success((refreshed) => {
        if (refreshed) {
          updateWatchVariables(true);
          setKeyclockToStore();
        }
      });
    }, 200000);

    (() => {
      keycloak
        .init(options)
        .success((authenticated) => {
          updateWatchVariables(authenticated);
          Object.defineProperty(Vue.prototype, '$keycloak', {
            get() {
              return keycloak;
            },
          });
          if (!authenticated) {
            keycloak.login();
          }
        })
        .error((err) => {
          throw new Error(`Erro na autenticação => StackTrace ${err}`);
        });
    })();
  });
}
