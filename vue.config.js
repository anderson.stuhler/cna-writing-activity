const path = require('path');
const alias = require('./aliases.config').webpack;

const isProduction = process.env.NODE_ENV === 'production';
const isE2ETests = process.env.CYPRESS === 'true';

function resolve(dir) {
  return path.join(__dirname, dir);
}

module.exports = {
  devServer: {
    proxy: {
      '/api': {
        target: 'https://stage-course-db1.cnago.com.br',
        changeOrigin: true,
        pathRewrite: {
          '^/api': '',
        },
      },
    },
  },
  css: {
    modules: isProduction && !isE2ETests,
    extract: !isProduction || isE2ETests,
    sourceMap: !isProduction || isE2ETests,
  },
  configureWebpack: {
    resolve: {
      alias,
    },
  },
  chainWebpack: (config) => {
    config.resolve.alias.set('@', resolve('src'));
  },
};
